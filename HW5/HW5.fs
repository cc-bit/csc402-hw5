module HW5
open System

type Fexpr =
 | Const of float
 | X
 | Add of Fexpr * Fexpr
 | Sub of Fexpr * Fexpr
 | Mul of Fexpr * Fexpr
 | Div of Fexpr * Fexpr
 | Sin of Fexpr
 | Cos of Fexpr
 | Log of Fexpr
 | Exp of Fexpr
 ;;
 
let rec toString = function
  | Const x -> string x
  | X -> "x"
  | Add(fe1,fe2) -> "(" + (toString fe1) + ")" + "+" + "(" + (toString fe2) + ")"
  | Sub(fe1,fe2) -> "(" + (toString fe1) + ")" + "-" + "(" + (toString fe2) + ")"
  | Mul(fe1,fe2) -> "(" + (toString fe1) + ")" + "*" + "(" + (toString fe2) + ")"
  | Div(fe1,fe2) -> "(" + (toString fe1) + ")" + "/" + "(" + (toString fe2) + ")"
  | Sin fe -> "sin(" + (toString fe) + ")"
  | Cos fe -> "cos(" + (toString fe) + ")"
  | Log fe -> "log(" + (toString fe) + ")"
  | Exp fe -> "exp(" + (toString fe) + ")"
  ;;

// Problem 1
let rec eval x = function
 | Const c ->  c
 | X ->  x
 | Add(fe1,fe2) ->  eval x fe1 + eval x fe2
 | Sub(fe1,fe2) ->  eval x fe1 - eval x fe2
 | Mul(fe1,fe2) ->  eval x fe1 * eval x fe2
 | Div(fe1,fe2) ->  eval x fe1 / eval x fe2
 | Sin fe -> Math.Sin(eval x fe)
 | Cos fe -> Math.Cos(eval x fe)
 | Log fe -> Math.Log(eval x fe)
 | Exp fe -> Math.Exp(eval x fe)
 ;;
// END of Problem 1

 
type BinTree<'a> =
 | Leaf
 | Node of BinTree<'a> * 'a * BinTree<'a>   
 ;;


// Problem 2
let rec levelOrderHelper = function
 | [] -> []
 | Leaf::subTrees -> levelOrderHelper subTrees
 | Node(tl,x,tr)::subTrees -> x :: levelOrderHelper [yield! subTrees; yield tl; yield tr]
 ;;
    
let levelOrder binTree = levelOrderHelper [binTree]
;;
// END of Problem 2



type SizedBinTree<'a> =
  | SizedLeaf
  | SizedNode of SizedBinTree<'a> * 'a * SizedBinTree<'a> * int   
let size = function
  | SizedLeaf -> 0
  | SizedNode(tl,x,tr,sz) -> sz
  ;;

// Problem 3
let rec binTree2SizedBinTree = function
    | Leaf -> SizedLeaf
    | Node(tl, x, tr) -> let rec leftSize = size(binTree2SizedBinTree(tl))
                         let rec rightSize = size(binTree2SizedBinTree(tr))
                         SizedNode(binTree2SizedBinTree(tl),x,binTree2SizedBinTree(tr),leftSize + 1 + rightSize)         
  ;;
// END of Problem 3

// Problem 4
let rec getOptionFromSizedBinTree index = function
    | SizedLeaf -> None
    | SizedNode(tl,x,tr,sz) -> if index = size(tl) then Some x
                               elif ( index < size(tl) ) then getOptionFromSizedBinTree index tl // recurse left
                               elif index > size(tl) then getOptionFromSizedBinTree (index - (size(tl) + 1)) tr // recurse right
                               else None
    ;;
// END of Problem 4



type Circuit<'a> =
 | Comp of 'a
 | Ser of Circuit<'a> * Circuit<'a>
 | Par of Circuit<'a> * Circuit<'a>

let rec circRec (c,s,p) = function
 | Comp x -> c x
 | Ser (c1,c2) -> s (circRec (c,s,p) c1) (circRec (c,s,p) c2)
 | Par (c1, c2) -> p (circRec (c,s,p) c1) (circRec (c,s,p) c2)


// Problem 5
let sum circ = circRec ((fun r -> r), (+), (+)) circ : float
;;
// END of Problem 5

// Problem 6
let sumAlongCheapestPath circ = circRec ((fun r -> r), (+), (fun r1 r2 -> Math.Min(r1 ,r2))) circ : float
;;
// END of Problem 6